variable "cird_block" {
    description = "",
    type = string,
    default = "10.0.0.0/16"
  
}
variable "ami_id" {
    description = "",
    type = string,
    default = "ami-09d3b3274b6c5d4aa"
}
variable "instance_type" {
    description = "",
    type = string, # Terraform 0.11 and earlier required type constraints to be given in quotes, but that form is now deprecated and will be removed in a future version of Terraform. Remove the     
│ ## quotes around "string".
    default = "t2.micro"
}
variable "subnet_cidr_block" {
    description = "the cidr for the subnets",
    type = list,
    default = [ "10.0.5.0/24", "10.0.3.0/24", "10.0.1.0/24", "10.0.7.0/24", "10.0.2.0/24", "10.0.4.0/24"  ]
}