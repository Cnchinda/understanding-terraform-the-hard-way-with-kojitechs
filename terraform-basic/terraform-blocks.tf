## the terraform block which restraint the version use to apply this code
terraform {
    required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider (provider block)
provider "aws" {
  region = "us-east-1"
  profile = "default" #$home/ .aws/credentials or "%USERPROFILE%\.aws\credentials" on windows

}
resource "aws_vpc" "vpc-my" {
    cidr_block = var.cird_block
    instance_tenancy = "default"
}

locals {
  example_vpc_id  = aws_vpc.example.id
}

resource "aws_instance" "my_ec2" {
      ami = var.ami_id
     instance_type = var.instance_type
        tags = {
       name = "my_ec2_1"
    }
  
}
# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

/* resource "aws_subnet" "example" {
  vpc_id            = data.aws_vpc.selected.id
  availability_zone = "us-west-2a"
  cidr_block        = cidrsubnet(data.aws_vpc.selected.cidr_block, 4, 1) */
}

# e.g. Create subnets in the first two available availability zones

resource "aws_subnet" "primary" {
  vpc_id     = aws_vpc.example.id # 
  cidr_block = var.subnet_cidr_block[5]
  availability_zone = data.aws_availability_zones.available.names[0]

  # ...
}

resource "aws_subnet" "secondary" {
   vpc_id     = aws_vpc.example.id # 
  cidr_block = var.subnet_cidr_block[4]
  availability_zone = data.aws_availability_zones.available.names[1]

  # ...
}

resource "aws_subnet" "private1" {
  vpc_id     = aws_vpc.example.id # 
  cidr_block = var.subnet_cidr_block[2] # var.variable 
  availability_zone =  data.aws_availability_zones.available.names[0]
}
 
resource "aws_subnet" "private2" {
  vpc_id     = aws_vpc.example.id # 
  cidr_block = var.subnet_cidr_block[1]
  availability_zone =  data.aws_availability_zones.available.names[1]
}

resource "aws_subnet" "private3" {
  vpc_id     = aws_vpc.example.id # 
  cidr_block = var.subnet_cidr_block[0]
  availability_zone =  data.aws_availability_zones.available.names[0]
}
 
resource "aws_subnet" "private4" {
  vpc_id     = aws_vpc.example.id # 
  cidr_block = var.subnet_cidr_block[3]
  availability_zone =  data.aws_availability_zones.available.names[1]
}


